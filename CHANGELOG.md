# v2.2 28th June 2021
- fixed redraw issue after setting a time offset

# v2.1 27th June 2021
- removed font selections that dont work with OS5

# v2.0 23rd June 2021
- initial release for OS5, for Versa 3+

# v1.5 25th September 2020
- updated sdk to 4.2.1 and tools to 1.7.3
- added support for Active Zone Minutes
- added extra dark colours
- added analytics for the device model name

# v1.4 5th March 2020
- updated sdk to 4.1.0 and tools to 1.7.1
- fix memory issue on launch

# v1.3 6th December 2019
- updated sdk to 4.0.1 and tools to 1.7
- fixed wide font layout on os 4

# v1.2 13th November 2019
- added "on device" settings screen
- added tumbler screen to enable the time offset to be set
- added interval start and end hour to settings

# v1.1 18th October 2019
- added analytics
- fixed a render issue when there are more than 100,000 steps
- added support for AM/PM indicator and narrow font display
- added optional secondary display to the side panel
- added interval alerts

# v1.0 5th September 2019
- initial release, no analytics