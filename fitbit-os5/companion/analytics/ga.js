var _this = this;
import * as tslib_1 from "tslib";
import { inbox } from "file-transfer";
var debug = true;
var send = function (message) {
    var e_1, _a, e_2, _b;
    var query = [
        "v=1",
        "tid=" + message.tracking_id,
        "cid=" + message.client_id,
        "t=" + message.hit_type,
        "sr=" + message.screen_resolution,
        "aip=" + message.anonymize_ip
    ];
    if (message.hit_type == "event") {
        query.push("ec=" + message.event_category);
        query.push("ea=" + message.event_action);
        query.push("el=" + message.event_label);
    }
    if (message.hit_type == "screenview") {
        query.push("cd=" + message.screen_name);
    }
    if (message.data_source) {
        query.push("ds=" + message.data_source);
    }
    if (message.user_language) {
        query.push("ul=" + message.user_language);
    }
    try {
        for (var _c = tslib_1.__values(message.custom_dimensions), _d = _c.next(); !_d.done; _d = _c.next()) {
            var dimension = _d.value;
            query.push("cd" + dimension.index + "=" + dimension.value);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
        }
        finally { if (e_1) throw e_1.error; }
    }
    try {
        for (var _e = tslib_1.__values(message.custom_metrics), _f = _e.next(); !_f.done; _f = _e.next()) {
            var metric = _f.value;
            query.push("cm" + metric.index + "=" + metric.value);
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
        }
        finally { if (e_2) throw e_2.error; }
    }
    var queue_time = Date.now() - message.timestamp;
    if (message.include_queue_time == "always" || (message.include_queue_time == "sometimes" && queue_time < 14400000)) {
        query.push("qt=" + queue_time);
    }
    debug && console.log("Query: " + query.join("&"));
    fetch("https://www.google-analytics.com/collect", {
        method: "POST",
        body: query.join("&")
    });
};
var process_files = function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var file, payload;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4, inbox.pop()];
            case 1:
                if (!(file = _a.sent())) return [3, 3];
                return [4, file.cbor()];
            case 2:
                payload = _a.sent();
                if (file.name.startsWith("_google_analytics_")) {
                    debug && console.log("File: " + file.name + " is being processed.");
                    send(payload);
                }
                return [3, 0];
            case 3: return [2];
        }
    });
}); };
inbox.addEventListener("newfile", process_files);
process_files();
