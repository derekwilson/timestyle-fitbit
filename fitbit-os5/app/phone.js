import * as messaging from "messaging";
import * as logging from "../common/logging";
import * as emptyObjects from "../common/objects";

const OPEN = messaging.peerSocket.OPEN;

let clientCallback = null;
let savedMessage = null;    // we will only save one message

export function initialize(callback) {
  clientCallback = callback;
  if (savedMessage != null) {
      logging.info("Phone - initialize saved message");
      clientCallback(savedMessage);
      savedMessage = null;
  }
}

export function is_phone_attached() {
  const readyState = messaging.peerSocket.readyState;
  const connected = (readyState === OPEN);
  logging.debug(`Phone - Connectivity status=${readyState} Connected? ${connected}`);
  return connected;
}

// Received message from the phone
messaging.peerSocket.addEventListener("message", function(evt) {
    logging.info("Phone - eventListener fired");
    if (clientCallback == null) {
        logging.info("addEventListener: callback not initialised - storing");
        if (savedMessage != null) {
            logging.warning("addEventListener: callback not initialised - storing - message discarded");
        }
        savedMessage = evt;
        return;
    }
    clientCallback(evt);
})

messaging.peerSocket.onopen = function() {
    logging.info("Message socket openned");
    if (clientCallback == null) {
        logging.info("onopen: callback not initialised - ignoring");
        return;
    }
    // Ready to send or receive messages
    clientCallback( 
        {
            "data":
            {
                "status": emptyObjects.status(true)
            }
        }
    );
};

// Listen for the onerror event
messaging.peerSocket.onerror = function(err) {
    // Handle any errors
    logging.info("Connection error: " + err.code + " - " + err.message);
    if (clientCallback == null) {
        logging.info("callback not initialised - ignoring");
        return;
    }
    clientCallback( 
        {
            "data":
            {
                "status": emptyObjects.status(false)
            }
        }
    );
}
  
export function send(data) {
    if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
        messaging.peerSocket.send(data);
    } else {
        logging.warning("trying to send when connection is not open");
        clientCallback( 
            {
                "isError": true,
                "message": "no phone",
                "data":
                {
                    "status": emptyObjects.status(false)
                }
            }
        );
    }
}
  
