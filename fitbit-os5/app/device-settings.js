/*
  Responsible for loading, applying and saving settings.
  Requires companion/simple/companion-settings.js
  Callback should be used to update your UI.
*/
import { me } from "appbit";
import * as fs from "fs";
import * as logging from "../common/logging";
import * as literals from "../common/literals";
import * as emptyObjects from "../common/objects";
import * as analytics from "./analytics"

const SETTINGS_TYPE = "cbor";
const SETTINGS_FILE = "settings.cbor";
const DEVICE_SETTINGS_FILE = "devicesettings.cbor";

let currentSettings = {};
let currentDeviceOnlySettings = {};
let onsettingschange;

export function initialize(callback) {
  currentSettings = loadSettings();
  setAllDefaultSettings();
  currentDeviceOnlySettings = loadDeviceSettings();
  setAllDefaultDeviceSettings();
  onsettingschange = callback;
  logging.info("settings initialised");
  logging.info("device only settings initialised " + JSON.stringify(currentDeviceOnlySettings));
  onsettingschange(currentSettings, currentDeviceOnlySettings);
}

export function reset(callback) {
  logging.info("settings reset");
  currentSettings = emptyObjects.default_settings();
  setAllDefaultSettings();
  currentDeviceOnlySettings = emptyObjects.default_device_settings();
  setAllDefaultDeviceSettings();
  onsettingschange = callback;
  logging.info("settings initialised");
  onsettingschange(currentSettings, currentDeviceOnlySettings);
}

function setAllDefaultSettings() {
  setDefaultSetting(literals.SETTING_TIME_COLOUR);
  setDefaultSetting(literals.SETTING_BACKGROUND_COLOUR);
  setDefaultSetting(literals.SETTING_PANEL_TEXT_COLOUR);
  setDefaultSetting(literals.SETTING_PANEL_BACKGROUND_COLOUR);
  setDefaultSetting(literals.SETTING_ZERO_PAD_HOUR);
  setDefaultSetting(literals.SETTING_ZERO_PAD_MONTH);
  setDefaultSetting(literals.SETTING_TIME_FORMAT);
  setDefaultSetting(literals.SETTING_ALERT_BT_STATUS_CHANGED);
  setDefaultSetting(literals.SETTING_SHOW_AMPM);
  setDefaultSetting(literals.SETTING_SHOW_STEPS);
}

function setDefaultSetting(key) {
  if (currentSettings[key] == null) {
    var defaultSettings = emptyObjects.default_settings();
    currentSettings[key] = defaultSettings[key];
  }
}

function setAllDefaultDeviceSettings() {
  setDefaultDeviceSetting(literals.DEVICE_SETTING_ALERT_SUPPRESS);
  setDefaultDeviceSetting(literals.DEVICE_SETTING_TIME_OVERRIDE);
  setDefaultDeviceSetting(literals.DEVICE_SETTING_TIME_OVERRIDE_PLUS);
  setDefaultDeviceSetting(literals.DEVICE_SETTING_TIME_OVERRIDE_HOUR);
  setDefaultDeviceSetting(literals.DEVICE_SETTING_TIME_OVERRIDE_MIN);
}

function setDefaultDeviceSetting(key) {
  if (currentDeviceOnlySettings[key] == null) {
    var defaultSettings = emptyObjects.default_device_settings();
    currentDeviceOnlySettings[key] = defaultSettings[key];
  }
}

function oneSettingChangedAnalytics(settings, newSettings, key, category, action, label) {
  if (settings[key] != newSettings[key]) {
    // the setting has changed
    logging.debug(`setting ${key} has changed from ${settings[key]} to ${newSettings[key]}`);
    analytics.sendAnalyticsEventAndLabel(category, action, label)
  }
}

function oneOptionSettingChangedAnalytics(settings, newSettings, key, category, action, label) {
  var oldValue = (settings[key] == null) ? -1 : parseInt(settings[key].selected);
  var newValue = (newSettings[key] == null) ? -1 : parseInt(newSettings[key].selected);
  if (oldValue != newValue) {
    // the setting has changed
    logging.debug(`setting ${key} has changed from ${oldValue} to ${newValue}`);
    analytics.sendAnalyticsEventAndLabel(category, action, label)
  }
}

function settingChangedAnalytics(settings, newSettings) {
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_BACKGROUND_COLOUR, 
    literals.AC_SETTING_COLOUR, literals.SETTING_BACKGROUND_COLOUR, newSettings[literals.SETTING_BACKGROUND_COLOUR]);
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_TIME_COLOUR, 
    literals.AC_SETTING_COLOUR, literals.SETTING_TIME_COLOUR, newSettings[literals.SETTING_TIME_COLOUR]);
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_PANEL_BACKGROUND_COLOUR, 
    literals.AC_SETTING_COLOUR, literals.SETTING_PANEL_BACKGROUND_COLOUR, newSettings[literals.SETTING_PANEL_BACKGROUND_COLOUR]);
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_PANEL_TEXT_COLOUR, 
    literals.AC_SETTING_COLOUR, literals.SETTING_PANEL_TEXT_COLOUR, newSettings[literals.SETTING_PANEL_TEXT_COLOUR]);

  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_ZERO_PAD_MONTH, 
    literals.AC_SETTING, literals.SETTING_ZERO_PAD_MONTH, newSettings[literals.SETTING_ZERO_PAD_MONTH]);
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_ZERO_PAD_HOUR, 
    literals.AC_SETTING, literals.SETTING_ZERO_PAD_HOUR, newSettings[literals.SETTING_ZERO_PAD_HOUR]);

  oneOptionSettingChangedAnalytics(settings, newSettings, literals.SETTING_TIME_FORMAT, 
    literals.AC_SETTING, literals.SETTING_TIME_FORMAT, 
    newSettings[literals.SETTING_TIME_FORMAT] == null ? "null" : newSettings[literals.SETTING_TIME_FORMAT].selected);

  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_ALERT_BT_STATUS_CHANGED, 
    literals.AC_SETTING, literals.SETTING_ALERT_BT_STATUS_CHANGED, newSettings[literals.SETTING_ALERT_BT_STATUS_CHANGED]);
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_SHOW_AMPM, 
    literals.AC_SETTING, literals.SETTING_SHOW_AMPM, newSettings[literals.SETTING_SHOW_AMPM]);
  oneSettingChangedAnalytics(settings, newSettings, literals.SETTING_SHOW_STEPS, 
    literals.AC_SETTING, literals.SETTING_SHOW_STEPS, newSettings[literals.SETTING_SHOW_STEPS]);

  oneOptionSettingChangedAnalytics(settings, newSettings, literals.SETTING_PANEL_BOTTOM_DISPLAY, 
    literals.AC_SETTING, literals.SETTING_PANEL_BOTTOM_DISPLAY, 
    newSettings[literals.SETTING_PANEL_BOTTOM_DISPLAY] == null ? "null" : newSettings[literals.SETTING_PANEL_BOTTOM_DISPLAY].selected);
  oneOptionSettingChangedAnalytics(settings, newSettings, literals.SETTING_ALERT_INTERVAL, 
    literals.AC_SETTING, literals.SETTING_ALERT_INTERVAL, 
    newSettings[literals.SETTING_ALERT_INTERVAL] == null ? "null" : newSettings[literals.SETTING_ALERT_INTERVAL].selected);

  oneOptionSettingChangedAnalytics(settings, newSettings, literals.SETTING_ALERT_INTERVAL_START, 
    literals.AC_SETTING, literals.SETTING_ALERT_INTERVAL_START, 
    newSettings[literals.SETTING_ALERT_INTERVAL_START] == null ? "null" : newSettings[literals.SETTING_ALERT_INTERVAL_START].selected);
  oneOptionSettingChangedAnalytics(settings, newSettings, literals.SETTING_ALERT_INTERVAL_END, 
    literals.AC_SETTING, literals.SETTING_ALERT_INTERVAL_END, 
    newSettings[literals.SETTING_ALERT_INTERVAL_END] == null ? "null" : newSettings[literals.SETTING_ALERT_INTERVAL_END].selected);
          

}

// settings changed on the phone
export function settingsChanged(evt) {
  //logging.debug("settings changed " + JSON.stringify(evt));
  if (evt.data != null && evt.data.settings != null) {
    settingChangedAnalytics(currentSettings, evt.data.settings);
    currentSettings = evt.data.settings;
    onsettingschange(currentSettings, currentDeviceOnlySettings);
  }
}

export function sendAnalytics(evt) {
  logging.debug("analytics " + evt.data.analytics.category);
  if (evt.data != null && evt.data.analytics != null) {
    analytics.sendAnalyticsEventAndLabel(evt.data.analytics.category, evt.data.analytics.action, evt.data.analytics.label);
  }
}

export function isZeroPadMonth() {
  return currentSettings[literals.SETTING_ZERO_PAD_MONTH];
}

export function isZeroPadHour() {
  return currentSettings[literals.SETTING_ZERO_PAD_HOUR];
}

export function isShowAmPm() {
  return currentSettings[literals.SETTING_SHOW_AMPM];
}

export function timeFormat() {
  return currentSettings[literals.SETTING_TIME_FORMAT];
}

export function alertOnBluetoothStatusChanged() {
  if (isSuppressAlerts()) {
    logging.debug("alertOnBluetoothStatusChanged suppressed");
    return false;
  }
  return currentSettings[literals.SETTING_ALERT_BT_STATUS_CHANGED];
}

export function selectedBottomDisplay() {
  if (currentSettings[literals.SETTING_PANEL_BOTTOM_DISPLAY] == null) {
    return literals.SETTING_PANEL_BOTTOM_DISPLAY_NONE;
  }
  return parseInt(currentSettings[literals.SETTING_PANEL_BOTTOM_DISPLAY].selected);
}

export function selectedIntervalAlert() {
  if (isSuppressAlerts()) {
    logging.debug("selectedIntervalAlert suppressed");
    return literals.SETTING_ALERT_INTERVAL_NONE;
  }
  if (currentSettings[literals.SETTING_ALERT_INTERVAL] == null) {
    return literals.SETTING_ALERT_INTERVAL_NONE;
  }
  return parseInt(currentSettings[literals.SETTING_ALERT_INTERVAL].selected);
}

export function selectedIntervalAlertStart() {
  if (currentSettings[literals.SETTING_ALERT_INTERVAL_START] == null) {
    return 0;
  }
  return parseInt(currentSettings[literals.SETTING_ALERT_INTERVAL_START].selected);
}

export function selectedIntervalAlertEnd() {
  if (currentSettings[literals.SETTING_ALERT_INTERVAL_END] == null) {
    return 24;
  }
  var endHour = parseInt(currentSettings[literals.SETTING_ALERT_INTERVAL_END].selected);
  // lets treat midnight as the next day
  return endHour == 0 ? 24 : endHour;
}

export function isDeviceSettingsBeingUsed() {
  return isSuppressAlerts() || isOverrideTime();
}

export function isSuppressAlerts() {
  return currentDeviceOnlySettings[literals.DEVICE_SETTING_ALERT_SUPPRESS];
}

export function isOverrideTime() {
  return currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE];
}

export function getOverridePlusMinus() {
  return currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_PLUS];
}

export function getOverrideMinsStr() {
  return currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_MIN];
}

export function getOverrideHoursStr() {
  return currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_HOUR];
}

export function getOverrideMins() {
  logging.debug(`getOverrideMins ${currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_HOUR]}, ${currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_MIN]}`);
  var retval = parseInt(currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_HOUR]) * 60;
  logging.debug(`getOverrideMins retval ${retval}`);
  retval = retval + parseInt(currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_MIN])
  logging.debug(`getOverrideMins retval ${retval}`);
  if (getOverridePlusMinus() == "-") {
    return retval * -1;
  }
  return retval;
}

export function setSuppressAlerts(flag) {
  currentDeviceOnlySettings[literals.DEVICE_SETTING_ALERT_SUPPRESS] = flag;
}

export function setOverrideTime(flag) {
  currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE] = flag;
}

export function setOverrideTimeOffset(plus, hour, min) {
  currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_PLUS] = plus;
  currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_HOUR] = hour;
  currentDeviceOnlySettings[literals.DEVICE_SETTING_TIME_OVERRIDE_MIN] = min;
}

// Register for the unload event
me.addEventListener("unload", saveSettings);

// Load settings from filesystem
function loadSettings() {
  try {
    return fs.readFileSync(SETTINGS_FILE, SETTINGS_TYPE);
  } catch (ex) {
    return emptyObjects.default_settings();
  }
}

// Load settings from filesystem
function loadDeviceSettings() {
  try {
    return fs.readFileSync(DEVICE_SETTINGS_FILE, SETTINGS_TYPE);
  } catch (ex) {
    return emptyObjects.default_device_settings();
  }
}

// Save settings to the filesystem
function saveSettings() {
  logging.debug("saving settings");
  fs.writeFileSync(SETTINGS_FILE, currentSettings, SETTINGS_TYPE);
  logging.debug("saving device only settings" + JSON.stringify(currentDeviceOnlySettings));
  fs.writeFileSync(DEVICE_SETTINGS_FILE, currentDeviceOnlySettings, SETTINGS_TYPE);
}
