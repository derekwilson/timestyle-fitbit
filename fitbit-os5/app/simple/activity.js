/*
  A simple way of returning activity data in the correct format based on user preferences.
  Callback should be used to update your UI.
*/
import { HeartRateSensor } from "heart-rate";
import { me } from "appbit";
import { display } from "display";
import clock from "clock";
import { today } from "user-activity";
import { units } from "user-settings";
import * as logging from "../../common/logging";

let activityCallback = null;

let hrmInUse = false;
let hrm = null;
let watchID = null;
let lastHeartRateReading = "--";
let lastReading = 0;

export function initialize(granularity, callback, needHrm) {
  if (me.permissions.granted("access_activity") &&
      me.permissions.granted("access_heart_rate") &&
      me.permissions.granted("access_user_profile")
      ) {
    setupHrm(needHrm);
    clock.granularity = granularity;
    clock.addEventListener("tick", tickHandler);
    activityCallback = callback;
  } else {
    logging.warning("Denied User Activity or hear rate permission");
    callback({
      steps: getDeniedStats(),
      heartRate: getDeniedStats(),
      distance: getDeniedStats(),
      elevationGain: getDeniedStats(),
      azm: getDeniedStats()
    });
  }
}

let activityData = () => {
  return {  
    steps: getSteps(),
    heartRate: getHeartRate(),
    distance: getDistance(),
    elevationGain: getElevationGain(),
    azm: getAzm()
  };  
}

export function refresh() {
  if (activityCallback != null) {
    activityCallback(activityData());
  }
}

function tickHandler(evt) {
  activityCallback(activityData());
}

export function setupHrm(inUse) {
  logging.debug(`HRM setup  ${inUse}`);
  if (hrm == null) {
    hrm = new HeartRateSensor();
  }
  hrmInUse = inUse;
  if (inUse) {
    setupEvents();
    start();
  } else {
    stop();
  }
}

function getHeartRateReading() {
  //logging.debug(`HRM reading  ${hrm.heartRate} , ${hrm.timestamp} `);
  if (!hrmInUse || hrm.heartRate == null || hrm.timestamp === lastReading) {
    lastHeartRateReading = "--";
    return;
  }
  lastHeartRateReading = hrm.heartRate;
  lastReading = hrm.timestamp;
}

function setupEvents() {
  display.addEventListener("change", function() {
    if (display.on) {
      start();
    } else {
      stop();
    }
  });
}

function start() {
  if (!watchID) {
    hrm.start();
    getHeartRateReading();
    watchID = setInterval(getHeartRateReading, 2000);
  }
}

function stop() {
  hrm.stop();
  clearInterval(watchID);
  watchID = null;
}

function getHeartRate() {
  return {
    pretty: lastHeartRateReading
  }
}

function convertDistanceToK(num, units) {
  if (num < 20) {
    return num.toFixed(1) + units 
  }
  return num.toFixed(0) + units 
}

function getDistance() {
  let val = (today.adjusted.distance || 0) / 1000;
  let u = "km";
  let uShort = "k";
  if(units.distance === "us") {
    val *= 0.621371;
    u = "mi";
    uShort = "m";
  }
  return {
    pretty: convertDistanceToK(val,uShort)
  }
}

function getElevationGain() {
  let val = today.adjusted.elevationGain || 0;
  return {
    pretty: val
  }
}

function convertNumberToK(num) {
  if (num > 999) {
    if (num > 100000) {
      return (num/1000).toFixed(0) + 'k' 
    } else {
      return (num/1000).toFixed(1) + 'k' 
    }
  }
  return num;
}

function getSteps() {
  let val = (today.adjusted.steps || 0);
  return {
    pretty: convertNumberToK(val)
  }
}

function getAzm() {
  let val = (today.adjusted.activeZoneMinutes.total || 0);
  return {
    pretty: convertNumberToK(val)
  }
}

function getDeniedStats() {
  return {
    pretty: "Denied"
  }
}