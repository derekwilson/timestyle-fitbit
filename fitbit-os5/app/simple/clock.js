/*
  A simple clock which renders the current time and date in a digital format.
  Callback should be used to update your UI.
*/
import clock from "clock";
import { preferences } from "user-settings";
import { vibration } from "haptics";

import { days } from "./locales/en.js";
import * as util from "./utils";
import * as logging from "../../common/logging";
import * as literals from "../../common/literals";
import * as analytics from "../analytics";

let dateFormat, zeroPadMonth, zeroPadHour, clockCallback;
let timeFormat = preferences.clockDisplay;
let applyOffsetTime = false;
let offsetTimeMins = 0;
let intervalAlertStartHour = 0;
let intervalAlertEndHour = 24;

export function initialize(granularity, dateFormatString, timeFormatSetting, padMonth, padHour, intervalSetting, intervalStart, intervalEnd, applyOffset, offsetMins, callback) {
  dateFormat = dateFormatString;
  setFormat(timeFormatSetting, padMonth, padHour, false);
  setOffset(applyOffset, offsetMins, false);
  clock.granularity = granularity;
  clockCallback = callback;
  clock.addEventListener("tick", tickHandler);
  tickHandler({date: new Date()});
  intervalAlertStartHour = intervalStart;
  intervalAlertEndHour = intervalEnd;
  handleIntervalAlert(intervalSetting);
}

export function setFormat(timeFormatSetting, padMonth, padHour, fireTickEvent) {
  //logging.verbose(`timeformat setting ${JSON.stringify(timeFormatSetting)}`)
  logging.verbose(`timeformat selection is zero ${timeFormatSetting.selected} ${timeFormatSetting.selected==0}`);
  timeFormat = convertTimeFormatSetting(timeFormatSetting.selected);
  logging.debug(`timeformat set to ${timeFormat}`);
  zeroPadMonth = padMonth;
  zeroPadHour = padHour;
  if (fireTickEvent && clockCallback) {
    tickHandler({date: new Date()});
  }
}

export function setOffset(applyOffset, offsetMins, fireTickEvent) {
  logging.debug(`setOffset ${applyOffset} ${offsetMins}`);
  applyOffsetTime = applyOffset;
  offsetTimeMins = offsetMins;
  if (fireTickEvent && clockCallback) {
    tickHandler({date: new Date()});
  }
}

export function setIntervalHours(intervalStart, intervalEnd) {
  intervalAlertStartHour = intervalStart;
  intervalAlertEndHour = intervalEnd;
}

function convertTimeFormatSetting(setting) {
  logging.verbose(`convertTimeFormatSetting setting ${setting}`);
  if (setting == literals.SETTING_TIME_FORMAT_12H) {
    return "12h";
  }
  if (setting == literals.SETTING_TIME_FORMAT_24H) {
    return "24h";
  }
  logging.debug(`preferences timeformat ${preferences.clockDisplay}`);
  return preferences.clockDisplay;
}

Date.prototype.addMinutes = function(mins) {
  this.setTime(this.getTime() + (mins*60*1000));
  return this;
}

function tickHandler(evt) {
  let today = evt.date;
  if (applyOffsetTime) {
    today.addMinutes(offsetTimeMins);
  }
  logging.debug(`offset ${offsetTimeMins} offsetNow ${today.toString()}`);

  let dayName = days[today.getDay()];
  let dayNumber = today.getDate();
  let dayNumberString = String(dayNumber);
  if (zeroPadMonth) {
    dayNumberString = util.zeroPad(dayNumber);
  } else {
    if (dayNumberString.substring(0,1) == "0") {
      dayNumberString = dayNumberString.substring(1);
    }
  }
  logging.verbose(`daynumber ${dayNumberString}`);

  let hours = today.getHours();
  logging.verbose(`timeformat ${timeFormat}`);
  if (timeFormat === "12h") {
    // 12h format
    hours = hours % 12 || 12;
  } else {
    // 24h format
    hours = hours;
  }
  hours = util.zeroPad(hours);
  let mins = util.zeroPad(today.getMinutes());

  let hours10Str = (zeroPadHour ? "0" : "");
  let hoursStr = String(hours);
  if (hours > 9) {
    hours10Str = hoursStr.substring(0,1);
    hoursStr = hoursStr.substring(1,2);
  } else {
    if (hoursStr.substring(0,1) == "0") {
      hoursStr = hoursStr.substring(1);
    }
  }
  let minsStr = String(mins);

  clockCallback(
    {
      hours10: hours10Str,
      hours: hoursStr,
      mins10: minsStr.substring(0,1),
      mins: minsStr.substring(1,2),
      day: dayName,
      dayNumber: dayNumberString,
      ampm: (today.getHours() < 12 ? "A" : "P")
    }
  );
}

export function handleIntervalAlert(settingValue) {
  let today = new Date()
  if (applyOffsetTime) {
    today.addMinutes(offsetTimeMins);
  }
  let mins = today.getMinutes();
  logging.debug(`handleIntervalAlert  ${settingValue}, ${mins}`);

  let interval = 0;
  let delay = 0;
  switch (settingValue) {
    case literals.SETTING_ALERT_INTERVAL_NONE:
      stop();
      return;
    case literals.SETTING_ALERT_INTERVAL_15:
      interval = 15 * 60 * 1000;
      delay = (mins == 0 ? 0 : ((15 - (mins % 15)) * 60 * 1000));
      break;
    case literals.SETTING_ALERT_INTERVAL_30:
      interval = 30 * 60 * 1000;
      delay = (mins == 0 ? 0 : ((30 - (mins % 30)) * 60 * 1000));
      break;
    case literals.SETTING_ALERT_INTERVAL_60:
      interval = 60 * 60 * 1000;
      delay = (mins == 0 ? 0 : ((60 - (mins % 60)) * 60 * 1000));
      break;
    default:
      logging.warning("handleIntervalAlert unknown type");
      stop();
      return;
  }
  start(settingValue, delay, interval);
}

let watchID = null;

function start(settingValue, delay, interval) {
  stop();
  logging.debug(`start interval ${settingValue}, delay ${delay/60000}, interval ${interval/60000}`);
  watchID = setTimeout(function() { intervalAlert(settingValue, interval); }, delay);
}

function stop() {
  clearTimeout(watchID);
  watchID = null;
}

function intervalAlert(settingValue, interval) {
  logging.info(`***intervalAlert - alert*** , ${intervalAlertStartHour} to ${intervalAlertEndHour}`);
  let today = new Date()
  if (applyOffsetTime) {
    today.addMinutes(offsetTimeMins);
  }
  if (today.getHours() >= intervalAlertStartHour && today.getHours() < intervalAlertEndHour) {
    vibration.stop();
    vibration.start("nudge-max");
    analytics.sendAnalyticsEventAndLabel(literals.AC_ALERT, literals.AA_ALERT_INTERVAL, settingValue);
  } else {
    logging.info("intervalAlert - suppressed as out of hours");
    analytics.sendAnalyticsEventAndLabel(literals.AC_ALERT, literals.AA_ALERT_INTERVAL_OUT_OF_HOURS, settingValue);
  }
  watchID = setTimeout(function() { intervalAlert(settingValue, interval); }, interval);
}


