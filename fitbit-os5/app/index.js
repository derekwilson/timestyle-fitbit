import document from "document";

import { me } from "appbit";
import { battery } from "power";
import { vibration } from "haptics";

import * as literals from "../common/literals";
import * as logging from "../common/logging";
import * as phone from "./phone";
import * as deviceSettings from "./device-settings";
import * as deviceUtil from "./device-utils";
import * as simpleClock from "./simple/clock";
import * as util from "./simple/utils";
import * as simpleActivity from "./simple/activity";
import * as analytics from "./analytics"

// settings for debugging
//logging.set_logging_level(logging.LOGGING_LEVEL_DEBUG);
//analytics.initialize(false, false);

// settings for the device
logging.set_logging_level(logging.LOGGING_LEVEL_INFO);
analytics.initialize(true, true);
deviceUtil.logMemoryStats("init");

analytics.sendAnalyticsEvent(literals.AC_LAUNCH, literals.APP_VERSION);

/////////////////////////
// settings screen
/////////////////////////
let settingsScreen = document.getElementById("settings-screen");
let settingsTitleBanner = settingsScreen.getElementById("heading-banner");
let settingsTitleLeft = settingsScreen.getElementById("heading-text-top-left");
let settingsTitleRight = settingsScreen.getElementById("heading-text-top-right");
let imageSuppressAlerts = null;
let imageOverrideTime = null;
let btnSettingsBack = document.getElementById("btn-settings-back");
let textOffset = null;

settingsTitleBanner.style.fill = (logging.getCurrentLogLevel() >= logging.LOGGING_LEVEL_DEBUG ? "red" : "darkblue");
settingsTitleLeft.text = `v${literals.APP_VERSION}`;

function toggleSuppressAlerts(chkImage) {
  // the settings module has an unload callback to save this state
  deviceSettings.setSuppressAlerts(!deviceSettings.isSuppressAlerts());
  logging.debug(`suppress alerts :: ${deviceSettings.isSuppressAlerts() ? "checked" : "unchecked"}`)
  analytics.sendAnalyticsEventAndLabel(
    literals.AC_DEVICE_SETTING, 
    literals.AA_SUPPRESS_ALERTS,
    deviceSettings.isSuppressAlerts()
    );
    setupCheckImageDisplay(chkImage, deviceSettings.isSuppressAlerts());
};

function toggleOverrideTime(chkImage) {
  // the settings module has an unload callback to save this state
  deviceSettings.setOverrideTime(!deviceSettings.isOverrideTime());
  logging.debug(`override time :: ${deviceSettings.isOverrideTime() ? "checked" : "unchecked"}`)
  analytics.sendAnalyticsEventAndLabel(
    literals.AC_DEVICE_SETTING, 
    literals.AA_OVERRIDE_TIME,
    deviceSettings.isOverrideTime()
    );
    setupCheckImageDisplay(chkImage, deviceSettings.isOverrideTime());
};


function setupCheckImageDisplay(image, chkValue) {
  if (image == null) {
    return;
  }
  if (chkValue) {
    image.style.display = "inline";
  } else {
    image.style.display = "none";
  }
}

function setupOffsetDisplay(display) {
  if (textOffset != null) {
    textOffset.text = display;
  }
}

// menu panel
let list = document.getElementById("settings-list");
let checkboxItems = list == null ? null : list.getElementsByClassName("checkbox-tile-item");
let menuItems = list == null ? null : list.getElementsByClassName("menu-list-item");

if (checkboxItems != null) {
  checkboxItems.forEach((element, index) => {
    let checkImage = element.getElementById("check-on-img");
    if (checkImage != null) {
      switch (index) {
        case 0:     // suppress alerts
          imageSuppressAlerts = checkImage;
          break;
        case 1:     // override time
          imageOverrideTime = checkImage;
          break;
      }
    }
    
    let touch = element.getElementById("touch");
    touch.onclick = (evt) => {
      logging.debug(`checkbox item: ${index}`);
      switch (index) {
        case 0:     // suppress alerts
          toggleSuppressAlerts(imageSuppressAlerts);
          break;
        case 1:     // override time
          toggleOverrideTime(imageOverrideTime);
          break;
      }
    }
  });
}

if (menuItems != null) {
    menuItems.forEach((element, index) => {
    let text2 = element.getElementById("text-line-2");
    if (text2 != null) {
      switch (index) {
        case 0:   // set offset
            textOffset = text2;
            setupOffsetDisplay("+00:00");
            break;
      }
    }
    let touch = element.getElementById("touch");
    touch.onclick = (evt) => {
      logging.debug(`menu item: ${index}`);
      switch (index) {
          case 0:     // set offset
              showOffsetScreen();
              break;
      }
    }
  });
}

btnSettingsBack.onclick = function(evt) {
  logging.debug("onclick settings back");
  showWatchFace();
  handleOverrideIndicator(deviceSettings.isDeviceSettingsBeingUsed());
  simpleClock.setOffset(deviceSettings.isOverrideTime(), deviceSettings.getOverrideMins(), true);
  // let the clock know the interval alert is set/not set - after we have updated the offset in case its changed
  simpleClock.handleIntervalAlert(deviceSettings.selectedIntervalAlert());
}

/////////////////////////
// offset screen
/////////////////////////

let offsetScreen = document.getElementById("offset-screen");
let btnOffsetBack = document.getElementById("btn-offset-back");

let tumbler1 = document.getElementById("offset-tumbler1");
let tumbler2 = document.getElementById("offset-tumbler2");
let tumbler3 = document.getElementById("offset-tumbler3");

tumbler1.addEventListener("select", function(evt) {
  logging.verbose("tumbler 1 select event fired"+"selectedIndex: " + tumbler1.value + " :: selectedValue: " + getTumblerText(tumbler1));
})
tumbler2.addEventListener("select", function(evt) {
  logging.verbose("tumbler 2 select event fired"+"selectedIndex: " + tumbler2.value + " :: selectedValue: " + getTumblerText(tumbler2));
})
tumbler2.addEventListener("select", function(evt) {
  logging.verbose("tumbler 3 select event fired"+"selectedIndex: " + tumbler3.value + " :: selectedValue: " + getTumblerText(tumbler3));
})

function getTumblerText(tumbler) {
  return tumbler.getElementById("item" + tumbler.value).getElementById("content").text;
}

function findIndexWithTumblerText(tumbler, text, defaultValue) {
  let retval = -1
  let items = tumbler.getElementsByClassName("tumbler-big-item");
  items.forEach((element, index) => {
    let itemText = element.getElementById("content");
    if (itemText != null) {
      if (itemText.text === text) {
        retval = index;
      }
    }
  })
  logging.debug(`findIndexWithTumblerText ${text} return = ${retval}`);
  return retval;
}

btnOffsetBack.onclick = function(evt) {
  logging.debug("onclick offset back");
  let display = getTumblerText(tumbler1) + getTumblerText(tumbler2) + ":" + getTumblerText(tumbler3);
  logging.info(`selected = ${display}`);
  setupOffsetDisplay(display);
  // the settings module has an unload callback to save this state
  deviceSettings.setOverrideTimeOffset(getTumblerText(tumbler1),getTumblerText(tumbler2),getTumblerText(tumbler3));
  showSettingsScreen();
}

/////////////////////////
// watch face screen
/////////////////////////
let watchFaceScreen = document.getElementById("watchface-screen");
let background = document.getElementById("background");
let txtAmPm = document.getElementById("txtAmPm");
txtAmPm.style.display = "none";   // start it off hidden
let txt10Hours = document.getElementById("txt10Hours");
let txtHours = document.getElementById("txtHours");
let txt10Mins = document.getElementById("txt10Mins");
let txtMins = document.getElementById("txtMins");
let panel = document.getElementById("panel");
let panelBackground = document.getElementById("panel-background");
let iconBluetooth = document.getElementById("bluetooth-icon");
let batteryLevel = document.getElementById("battery-level");
let iconBattery = document.getElementById("battery-icon");
let txtBattery = document.getElementById("battery-text");
let iconSteps = document.getElementById("steps-icon");
let txtSteps = document.getElementById("steps-text");
let iconSecondary = document.getElementById("secondary-icon");
let txtSecondary = document.getElementById("secondary-text");
let panelText = document.getElementsByClassName("panel-text");
let timeText = document.getElementsByClassName("time-text");
let dayText = document.getElementById("day-text");
let dateText = document.getElementById("date-text");

// this only needs to be done once
txtHours.x = panelBackground.getBBox().x;
txtMins.x = panelBackground.getBBox().x;

////////////////////////////////
// navigation
////////////////////////////////

panel.onclick = function(evt) {
  logging.debug("panel onclick");
  showSettingsScreen();
}

function showSettingsScreen() {
  settingsScreen.style.display = "inline";
  watchFaceScreen.style.display = "none";
  offsetScreen.style.display = "none";
}

function showWatchFace() {
  watchFaceScreen.style.display = "inline";
  settingsScreen.style.display = "none";
  offsetScreen.style.display = "none";
}

function showOffsetScreen() {
  offsetScreen.style.display = "inline";
  watchFaceScreen.style.display = "none";
  settingsScreen.style.display = "none";
}

///////////////////////////////
// display
///////////////////////////////

function updateBluetoothStatus(connected) {
  var newHref = "images/nobluetooth.png";
  if (connected) {
    newHref = "images/bluetooth.png";
  }
  if (newHref !== iconBluetooth.href) {
    logging.debug(`bluetooth status changed ${newHref}, alert = ${deviceSettings.alertOnBluetoothStatusChanged()}`);
    if (deviceSettings.alertOnBluetoothStatusChanged()) {
      vibration.stop();
      vibration.start("nudge-max");
    } 
  }
  iconBluetooth.href = newHref;
}

function realignDigits() {
  // re-align the 10s digits to the right of the units
  logging.debug(`realignDigits`);
  txt10Hours.x = txtHours.getBBox().x + 12;
  txt10Mins.x = txtMins.getBBox().x + 12;
}

function handleOverrideIndicator(overrideOn) {
  var indicator = txtAmPm.text;
  if (overrideOn) {
    // force the display on
    txtAmPm.style.display = "inline";
    if (indicator.length > 0 && indicator.charAt(0) !== literals.OVERRIDE_INDICATOR) {
      indicator = literals.OVERRIDE_INDICATOR + indicator;
    } else {
      indicator = literals.OVERRIDE_INDICATOR;
    }
    txtAmPm.text = indicator;
  } else {
    txtAmPm.style.display = (deviceSettings.isShowAmPm() == true ? "inline" : "none");
    if (indicator.length > 1 && indicator.charAt(0) === literals.OVERRIDE_INDICATOR) {
      indicator = indicator.substring(1);
    }
    txtAmPm.text = indicator;
  }
  logging.debug(`handleOverrideIndicator ${overrideOn}, indicator = ${indicator}`);
}

function handleSecondaryDisplay(settingValue) {
  logging.debug("handleSecondaryDisplay = " + settingValue);

  switch (parseInt(settingValue)) {
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_NONE:
      simpleActivity.setupHrm(false);
      break;
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_HEART_RATE:
      iconSecondary.href = "images/heartrate.png";
      simpleActivity.setupHrm(true);
      break;  
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_DISTANCE:
      iconSecondary.href = "images/distance.png";
      simpleActivity.setupHrm(false);
      break;  
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_FLOORS:
      iconSecondary.href = "images/floors.png";
      simpleActivity.setupHrm(false);
      break;  
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_AZM:
      iconSecondary.href = "images/azm.png";
      simpleActivity.setupHrm(false);
      break;  
    default:
      logging.warning("handleSecondaryDisplay unknown type");
      simpleActivity.setupHrm(false);
  };

  if (settingValue == literals.SETTING_PANEL_BOTTOM_DISPLAY_NONE) {
    logging.debug("handleSecondaryDisplay hiding icons");
    iconSecondary.style.display = "none";
    txtSecondary.style.display = "none";
  } else {
    iconSecondary.style.display = "inline";
    txtSecondary.style.display = "inline";
    txtSecondary.text = "";
    simpleActivity.refresh();
  }
}

///////////////////////////////
// callbacks
///////////////////////////////

/* -------- DEVICE SETTINGS -------- */
function settingsCallback(data, deviceOnlyData) {
  if (!data) {
    return;
  }
  //logging.debug("settingsCallback device only " + JSON.stringify(deviceOnlyData));
  setupCheckImageDisplay(imageSuppressAlerts, deviceOnlyData[literals.DEVICE_SETTING_ALERT_SUPPRESS]);
  setupCheckImageDisplay(imageOverrideTime, deviceOnlyData[literals.DEVICE_SETTING_TIME_OVERRIDE]);
  tumbler1.value = findIndexWithTumblerText(tumbler1,deviceSettings.getOverridePlusMinus(),0);
  tumbler2.value = findIndexWithTumblerText(tumbler2,deviceSettings.getOverrideHoursStr(),0);
  tumbler3.value = findIndexWithTumblerText(tumbler3,deviceSettings.getOverrideMinsStr(),0);
  var display = deviceSettings.getOverridePlusMinus() + deviceSettings.getOverrideHoursStr() + ":" + deviceSettings.getOverrideMinsStr();
  logging.debug(`offset display = ${display}`);
  setupOffsetDisplay(display);

  if (data[literals.SETTING_BACKGROUND_COLOUR]) {
    background.style.fill = data[literals.SETTING_BACKGROUND_COLOUR];
  }
  if (data[literals.SETTING_TIME_COLOUR]) {
    logging.verbose("number of time text items = " + timeText.length);
    timeText.forEach(item => {
      item.style.fill = data[literals.SETTING_TIME_COLOUR];
    });
  }
  if (data[literals.SETTING_PANEL_BACKGROUND_COLOUR]) {
    panelBackground.style.fill = data[literals.SETTING_PANEL_BACKGROUND_COLOUR];
  }
  if (data[literals.SETTING_PANEL_TEXT_COLOUR]) {
    batteryLevel.style.fill = data[literals.SETTING_PANEL_TEXT_COLOUR]; 
    iconBattery.style.fill = data[literals.SETTING_PANEL_TEXT_COLOUR];
    iconSteps.style.fill = data[literals.SETTING_PANEL_TEXT_COLOUR];
    iconSecondary.style.fill = data[literals.SETTING_PANEL_TEXT_COLOUR];
    iconBluetooth.style.fill = data[literals.SETTING_PANEL_TEXT_COLOUR];
    logging.verbose("number of panel text items = " + panelText.length);
    panelText.forEach(item => {
      item.style.fill = data[literals.SETTING_PANEL_TEXT_COLOUR];
    });
  }
  // this also handles the AM PM indicator
  handleOverrideIndicator(deviceSettings.isDeviceSettingsBeingUsed());
  if (data[literals.SETTING_SHOW_STEPS] != null) {
    logging.verbose("show steps " + data[literals.SETTING_SHOW_STEPS]);
    iconSteps.style.display = (data[literals.SETTING_SHOW_STEPS] == true ? "inline" : "none");
    txtSteps.style.display = (data[literals.SETTING_SHOW_STEPS] == true ? "inline" : "none");
  }
  if (data[literals.SETTING_PANEL_BOTTOM_DISPLAY] != null) {
    handleSecondaryDisplay(data[literals.SETTING_PANEL_BOTTOM_DISPLAY].selected);
  }
  simpleClock.setIntervalHours(deviceSettings.selectedIntervalAlertStart(),deviceSettings.selectedIntervalAlertEnd());

  // no need to kick the tickhandler as its done in the next line by setFormat
  simpleClock.setOffset(deviceSettings.isOverrideTime(), deviceSettings.getOverrideMins(), false);

  if (data[literals.SETTING_ZERO_PAD_HOUR] != null &&
      data[literals.SETTING_ZERO_PAD_MONTH] != null &&
      data[literals.SETTING_TIME_FORMAT] != null) {
    simpleClock.setFormat(
      data[literals.SETTING_TIME_FORMAT], 
      data[literals.SETTING_ZERO_PAD_MONTH], 
      data[literals.SETTING_ZERO_PAD_HOUR],
      true);
  }
  // let the clock know the interval alert is set/not set - after we have updated the offset
  simpleClock.handleIntervalAlert(deviceSettings.selectedIntervalAlert());
}

/* ------- PHONE STATUS --------- */
function phoneCallback(data) {
  // message received
  if (data.data != null && data.data.settings != null) {
    // settings changed on the phone
    logging.debug("phoneCallback settings");
    deviceSettings.settingsChanged(data);
  }
  if (data.data != null && data.data.analytics != null) {
    // we have some analytics to send
    logging.debug("phoneCallback analytics");
    deviceSettings.sendAnalytics(data);
  }
  if (data.data != null && data.data.status != null) {
    // status changed
    logging.debug("phoneCallback status ");
    updateBluetoothStatus(data.data.status[literals.STATUS_PHONE_ATTACHED]);
  }
}

/* --------- CLOCK ---------- */
function clockCallback(data) {
  //logging.debug("clockCallback " + JSON.stringify(data));
  logging.debug("clockCallback");

  // main time display
  txt10Hours.text = data.hours10;
  txtHours.text = data.hours;
  txt10Mins.text = data.mins10;
  txtMins.text = data.mins;
  dayText.text = data.day;
  dateText.text = data.dayNumber;
  if (deviceSettings.isShowAmPm()) {
    txtAmPm.text = data.ampm;
  } else {
    txtAmPm.text = "";
  }
  handleOverrideIndicator(deviceSettings.isDeviceSettingsBeingUsed());

  /** - for testing 
  txt10Hours.text = "2";
  txtHours.text = "8";
  txt10Mins.text = "2";
  txtMins.text = "8";
  dayText.text = "MON" 
  dateText.text = "30";
  */

  /* - for screenshot
  txt10Hours.text = "";
  txtHours.text = "6";
  txt10Mins.text = "2";
  txtMins.text = "3";
  dayText.text = "FRI" 
  dateText.text = "9";
  */

  realignDigits();

  // side panel
  var level = Math.floor(battery.chargeLevel);
  txtBattery.text = level + "%";
  batteryLevel.width = level / 2;
  updateBluetoothStatus(phone.is_phone_attached());

  // settings screen - the only place we show the real time
  var now = new Date();
  settingsTitleRight.text = `${now.getHours()}:${util.zeroPad(now.getMinutes())}`;
  // show memory instead
  //settingsTitleRight.text = `${deviceUtil.getJsMemoryUsed()}`;
}

/* ------- ACTIVITY --------- */
function activityCallback(data) {
  logging.debug("activityCallback");
  txtSteps.text = data.steps.pretty;

  switch (deviceSettings.selectedBottomDisplay()) {
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_NONE:
      break;
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_HEART_RATE:
      txtSecondary.text = data.heartRate.pretty;
      break;  
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_DISTANCE:
      txtSecondary.text = data.distance.pretty;
      break;  
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_FLOORS:
      txtSecondary.text = data.elevationGain.pretty;
      break;  
    case literals.SETTING_PANEL_BOTTOM_DISPLAY_AZM:
      txtSecondary.text = data.azm.pretty;
      break;  
    default:
      logging.warning("handleSecondaryDisplay unknown type");
      txtSecondary.text = "";
  }
}


///////////////////////////////
// init
///////////////////////////////
function initClockface() {
  logging.info("main: init device settings");
  deviceUtil.logMemoryStats("init device settings");
  try {
    deviceSettings.initialize(settingsCallback);
  } catch (ex) {
    logging.error("failed to initialise settings");
    analytics.sendAnalyticsEvent(literals.AC_ERROR, literals.AA_LOAD_SETTINGS);
    deviceSettings.reset(settingsCallback);
  }

  logging.info("main: init clock");
  deviceUtil.logMemoryStats("init clock");
  simpleClock.initialize(
    "minutes", 
    "longDate", 
    deviceSettings.timeFormat(),
    deviceSettings.isZeroPadMonth(), 
    deviceSettings.isZeroPadHour(),
    deviceSettings.selectedIntervalAlert(),
    deviceSettings.selectedIntervalAlertStart(),
    deviceSettings.selectedIntervalAlertEnd(),
    deviceSettings.isOverrideTime(), 
    deviceSettings.getOverrideMins(),
    clockCallback);

  logging.info("main: init phone");
  deviceUtil.logMemoryStats("init phone");
  phone.initialize(phoneCallback);
  
  logging.info("main: init activity");
  simpleActivity.initialize("minutes", activityCallback, parseInt(deviceSettings.selectedBottomDisplay())==literals.SETTING_PANEL_BOTTOM_DISPLAY_HEART_RATE);
}

// delay the init until the app is launched
setTimeout(function() { initClockface(); }, 1);

console.log(`timestyle-fitbit v${literals.APP_VERSION} started, ${me.buildId}, ${me.launchArguments}`);
deviceUtil.logMemoryStats("init complete");
