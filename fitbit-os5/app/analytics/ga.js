import { me as appbit } from "appbit";
import { encode } from "cbor";
import { me as device } from "device";
import { display } from "display";
import { outbox } from "file-transfer";
import { readFileSync, writeFileSync } from "fs";
var debug = false;
try {
    var client_id = readFileSync("_google_analytics_client_id", "cbor");
}
catch (error) {
    var client_id = Math.floor(Math.random() * 10000000000000000);
    writeFileSync("_google_analytics_client_id", client_id, "cbor");
}
var tracking_id = null;
var data_source = null;
var user_language = null;
var anonymize_ip = 1;
var custom_dimensions = [];
var custom_metrics = [];
var include_queue_time = "sometimes";
var configure = function (options) {
    tracking_id = options.tracking_id || tracking_id;
    data_source = options.data_source || data_source;
    user_language = options.user_language || user_language;
    anonymize_ip = options.anonymize_ip === 0 ? 0 : anonymize_ip;
    custom_dimensions = options.custom_dimensions || custom_dimensions;
    custom_metrics = options.custom_metrics || custom_metrics;
    include_queue_time = options.include_queue_time || include_queue_time;
    debug = options.debug_logging;
    onload();
};
var send = function (options) {
    debug && console.log("Tracking ID: " + tracking_id);
    debug && console.log("Client ID: " + client_id);
    var data = options;
    data.tracking_id = tracking_id;
    data.client_id = client_id;
    data.data_source = data_source;
    data.user_language = user_language;
    data.anonymize_ip = anonymize_ip;
    data.include_queue_time = include_queue_time;
    data.screen_resolution = device.screen ? (device.screen.width + "x" + device.screen.height) : "348x250";
    data.timestamp = Date.now();
    data.custom_dimensions = options.custom_dimensions ? custom_dimensions.concat(options.custom_dimensions) : custom_dimensions;
    data.custom_metrics = options.custom_metrics ? custom_metrics.concat(options.custom_metrics) : custom_metrics;
    var filename = "_google_analytics_" + (Math.floor(Math.random() * 10000000000000000));
    outbox.enqueue(filename, encode(data)).then(function () {
        debug && console.log("File: " + filename + " transferred successfully.");
    }).catch(function (error) {
        debug && console.log("File: " + filename + " failed to transfer.");
    });
};
var onload = function () {
    send({
        hit_type: "event",
        event_category: "Lifecycle",
        event_action: "Load",
        event_label: device.modelName
    });
};
display.addEventListener("change", function () {
    if (display.on) {
        send({
            hit_type: "event",
            event_category: "Display",
            event_action: "On",
            event_label: device.modelName
        });
    }
});
appbit.addEventListener("unload", function () {
    send({
        hit_type: "event",
        event_category: "Lifecycle",
        event_action: "Unload",
        event_label: device.modelName
    });
});
var analytics = {
    configure: configure,
    send: send
};
export default analytics;
