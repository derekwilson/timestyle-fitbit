import * as literals from "./literals";

// app settings
export function default_settings () {
    var out = {
        [literals.SETTING_BACKGROUND_COLOUR]:"black",
        [literals.SETTING_TIME_COLOUR]:"white",
        [literals.SETTING_PANEL_BACKGROUND_COLOUR]:"grey",
        [literals.SETTING_PANEL_TEXT_COLOUR]:"white",
        [literals.SETTING_ZERO_PAD_MONTH]:false,
        [literals.SETTING_ZERO_PAD_HOUR]:false,
        // this is how selections are formatted in the Fitbit settings JSX 
        [literals.SETTING_TIME_FORMAT]:{"values":[{"name":"Automatic","value":"auto"}],"selected":literals.SETTING_TIME_FORMAT_AUTO},
        [literals.SETTING_ALERT_BT_STATUS_CHANGED]:true,
        [literals.SETTING_SHOW_AMPM]:false,
        [literals.SETTING_SHOW_STEPS]:true,
        // this is how selections are formatted in the Fitbit settings JSX 
        [literals.SETTING_PANEL_BOTTOM_DISPLAY]:{"values":[{"name":"None","value":"none"}],"selected":literals.SETTING_PANEL_BOTTOM_DISPLAY_NONE},
        // this is how selections are formatted in the Fitbit settings JSX 
        [literals.SETTING_ALERT_INTERVAL]:{"values":[{"name":"None","value":literals.SETTING_ALERT_INTERVAL_NONE}],"selected":literals.SETTING_ALERT_INTERVAL_NONE},
        // this is how selections are formatted in the Fitbit settings JSX 
        [literals.SETTING_ALERT_INTERVAL_START]:{"values":[{"name":"Midnight","value":0}],"selected":0},
        // this is how selections are formatted in the Fitbit settings JSX 
        [literals.SETTING_ALERT_INTERVAL_END]:{"values":[{"name":"Midnight","value":0}],"selected":0}
    };
    out[literals.SETTING_VERSION] = literals.APP_VERSION;
    return out;
}

export function default_device_settings () {
    var out = {
        [literals.DEVICE_SETTING_ALERT_SUPPRESS]:false,
        [literals.DEVICE_SETTING_TIME_OVERRIDE]:false,
        [literals.DEVICE_SETTING_TIME_OVERRIDE_PLUS]:"+",
        [literals.DEVICE_SETTING_TIME_OVERRIDE_HOUR]:"00",
        [literals.DEVICE_SETTING_TIME_OVERRIDE_MIN]:"00"
    };
    out[literals.SETTING_VERSION] = literals.APP_VERSION;
    return out;
}

export function status (phoneAttached) {
    var out = {
        [literals.STATUS_PHONE_ATTACHED]: phoneAttached
    };
    return out;
}

