import * as literals from "../common/literals";
import * as logging from "../common/logging";

logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);

console.log("Opening timestyle-fitbit settings page");

const hourOptions = [
  {name:"Midnight", value:0},
  {name:"1 am", value:1},
  {name:"2 am", value:2},
  {name:"3 am", value:3},
  {name:"4 am", value:4},
  {name:"5 am", value:5},
  {name:"6 am", value:6},
  {name:"7 am", value:7},
  {name:"8 am", value:8},
  {name:"9 am", value:9},
  {name:"10 am", value:10},
  {name:"11 am", value:11},
  {name:"Noon", value:12},
  {name:"1 pm", value:13},
  {name:"2 pm", value:14},
  {name:"3 pm", value:15},
  {name:"4 pm", value:16},
  {name:"5 pm", value:17},
  {name:"6 pm", value:18},
  {name:"7 pm", value:19},
  {name:"8 pm", value:20},
  {name:"9 pm", value:21},
  {name:"10 pm", value:22},
  {name:"11 pm", value:23}
];

const colorSet = [
  {color: "black"},
  {color: "darkslategrey"},
  {color: "dimgrey"},
  {color: "grey"},
  {color: "lightgrey"},
  {color: "beige"},
  {color: "white"},
  {color: "maroon"},
  {color: "saddlebrown"},
  {color: "darkgoldenrod"},
  {color: "goldenrod"},
  {color: "rosybrown"},
  {color: "wheat"},
  {color: "navy"},
  {color: "blue"},
  {color: "dodgerblue"},
  {color: "deepskyblue"},
  {color: "aquamarine"},
  {color: "cyan"},
  {color: "olive"},
  {color: "darkgreen"},
  {color: "green"},
  {color: "springgreen"},
  {color: "limegreen"},
  {color: "palegreen"},
  {color: "lime"},
  {color: "greenyellow"},
  {color: "darkslateblue"},
  {color: "slateblue"},
  {color: "purple"},
  {color: "fuchsia"},
  {color: "plum"},
  {color: "orchid"},
  {color: "lavender"},
  {color: "darkkhaki"},
  {color: "khaki"},
  {color: "lemonchiffon"},
  {color: "yellow"},
  {color: "gold"},
  {color: "orangered"},
  {color: "orange"},
  {color: "coral"},
  {color: "lightpink"},
  {color: "palevioletred"},
  {color: "deeppink"},
  {color: "darkred"},
  {color: "crimson"},
  {color: "red"},
  {color: "#52351F"},
  {color: "#65500F"},
  {color: "#0E194F"}
];

const colourOptions = [
  ['Time Background Color', literals.SETTING_BACKGROUND_COLOUR],
  ['Time Text Color', literals.SETTING_TIME_COLOUR],
  ['Side Panel Background Color', literals.SETTING_PANEL_BACKGROUND_COLOUR],
  ['Side Panel Text Color', literals.SETTING_PANEL_TEXT_COLOUR]
];

function settingsComponent(props) {
  return (
    <Page>
      <Section title="TimeStyle">
        <Text bold align="center">v{literals.APP_VERSION}</Text>
        <Text align="center">More device settings can be accessed by tapping the side panel on the right of your watch face.</Text>
      </Section>
      <Section title="Alerts">
        <Toggle
            settingsKey={literals.SETTING_ALERT_BT_STATUS_CHANGED}
            label="Bluetooth status changes"
        />
        <Select
          label="Interval alert"
          settingsKey={literals.SETTING_ALERT_INTERVAL}
          options={[
            {name:"None", value:literals.SETTING_ALERT_INTERVAL_NONE},
            {name:"At 00, 15, and 30 minutes", value: literals.SETTING_ALERT_INTERVAL_15},
            {name:"At 00, and 30 minutes", value: literals.SETTING_ALERT_INTERVAL_30},
            {name:"On the hour", value: literals.SETTING_ALERT_INTERVAL_60}
          ]}
        />
        <Select
          label="Interval alert start"
          settingsKey={literals.SETTING_ALERT_INTERVAL_START}
          options={hourOptions}
        />
        <Select
          label="Interval alert end"
          settingsKey={literals.SETTING_ALERT_INTERVAL_END}
          options={hourOptions}
        />
      </Section>
      <Section title="Time Display">
        <Select
          label="Time format"
          settingsKey={literals.SETTING_TIME_FORMAT}
          options={[
            {name:"Automatic", value:literals.SETTING_TIME_FORMAT_AUTO},
            {name:"12 Hour", value: "12h"},
            {name:"24 Hour", value: "24h"}
          ]}
        />
        <Toggle
            settingsKey={literals.SETTING_ZERO_PAD_MONTH}
            label="Zero pad date day"
          />
        <Toggle
            settingsKey={literals.SETTING_ZERO_PAD_HOUR}
            label="Zero pad hour"
          />
        <Toggle
            settingsKey={literals.SETTING_SHOW_AMPM}
            label="Show AM/PM indicator"
          />
      </Section>
      <Section title="Side Panel Display">
        <Toggle
            settingsKey={literals.SETTING_SHOW_STEPS}
            label="Show steps"
          />
        <Select
          label="Secondary Display"
          settingsKey={literals.SETTING_PANEL_BOTTOM_DISPLAY}
          options={[
            {name:"None", value:literals.SETTING_PANEL_BOTTOM_DISPLAY_NONE},
            {name:"Heart Rate", value: literals.SETTING_PANEL_BOTTOM_DISPLAY_HEART_RATE},
            {name:"Distance", value: literals.SETTING_PANEL_BOTTOM_DISPLAY_DISTANCE},
            {name:"Floors", value: literals.SETTING_PANEL_BOTTOM_DISPLAY_FLOORS},
            {name:"Active Zone Minutes", value: literals.SETTING_PANEL_BOTTOM_DISPLAY_AZM}
          ]}
        />
      </Section>
      {colourOptions.map(([title, settingsKey]) =>
        <Section
          title={title}>
          <ColorSelect
            settingsKey={settingsKey}
            colors={colorSet} />
        </Section>
      )}
      <Text bold align="center">Licenses</Text>
      <Section title="TimeStylePebble">
        <Text>MIT License Copyright (c) Dan Tilden.</Text>
        <Link source="https://github.com/freakified/TimeStylePebble/blob/master/LICENSE">https://github.com/freakified/TimeStylePebble/blob/master/LICENSE</Link>
      </Section>
      <Section title="Design Asset License">
        <Text>Copyright (c) Fitbit, Inc.</Text>
        <Link source="https://github.com/Fitbit/sdk-design-assets/blob/master/LICENCE.txt">https://github.com/Fitbit/sdk-design-assets/blob/master/LICENCE.txt</Link>
      </Section>
      <Section title="SDK Moment">
        <Text>MIT License Copyright (c) 2018 Fitbit, Inc</Text>
        <Link source="https://github.com/Fitbit/sdk-moment/blob/master/LICENSE">https://github.com/Fitbit/sdk-moment/blob/master/LICENSE</Link>
      </Section>
    </Page>
  );
}

registerSettingsPage(settingsComponent);

