// app version
export const APP_VERSION = "1.5 step1";

export const JS_MEMORY_ALERT_LEVEL = 60000;

// analytics
export const ANALYTICS_TRACKING_ID = "UA-149964332-1";
export const ANALYTICS_DATA_SOURCE = "timestyle-fitbit-" + APP_VERSION;

export const AC_LAUNCH = "Launch";
export const AC_UPGRADE = "Upgrade";
export const AC_ALERT = "Alert";
export const AC_SETTING = "Setting";
export const AC_SETTING_COLOUR = "SettingColour";
export const AC_DEVICE_SETTING = "DeviceSetting";
export const AC_ERROR = "Error";

export const AA_ALERT_INTERVAL = "Interval";
export const AA_ALERT_INTERVAL_OUT_OF_HOURS = "IntervalOutOfHours";
export const AA_SUPPRESS_ALERTS = "SuppressAlerts";
export const AA_OVERRIDE_TIME = "OverrideTime";
export const AA_LOAD_SETTINGS = "LoadSettings";
export const AA_MEMORY = "Memory";

export const STATUS_PHONE_ATTACHED = "phoneAttached";

// phone settings
export const SETTING_BACKGROUND_COLOUR = "colorBackground";
export const SETTING_TIME_COLOUR = "colorTime";
export const SETTING_PANEL_BACKGROUND_COLOUR = "colorPanel";
export const SETTING_PANEL_TEXT_COLOUR = "colorPanelText";
export const SETTING_ZERO_PAD_MONTH = "zeroPadMonth";
export const SETTING_ZERO_PAD_HOUR = "zeroPadHour";
export const SETTING_TIME_FORMAT = "timeFormat";
export const SETTING_ALERT_BT_STATUS_CHANGED = "alertBtStatus";
export const SETTING_VERSION = "version";
export const SETTING_SHOW_AMPM = "showAmPm";
export const SETTING_NARROW_TIME_FONT = "narrowTimeFont";
export const SETTING_SHOW_STEPS = "showSteps";
export const SETTING_PANEL_BOTTOM_DISPLAY = "bottomDisplay";
export const SETTING_ALERT_INTERVAL = "alertInterval";
export const SETTING_ALERT_INTERVAL_START = "alertIntervalStart";
export const SETTING_ALERT_INTERVAL_END = "alertIntervalEnd";

export const SETTING_TIME_FORMAT_AUTO = 0;
export const SETTING_TIME_FORMAT_12H = 1;
export const SETTING_TIME_FORMAT_24H = 2;

export const SETTING_PANEL_BOTTOM_DISPLAY_NONE = 0;
export const SETTING_PANEL_BOTTOM_DISPLAY_HEART_RATE = 1;
export const SETTING_PANEL_BOTTOM_DISPLAY_DISTANCE = 2;
export const SETTING_PANEL_BOTTOM_DISPLAY_FLOORS = 3;
export const SETTING_PANEL_BOTTOM_DISPLAY_AZM = 4;

export const SETTING_ALERT_INTERVAL_NONE = 0;
export const SETTING_ALERT_INTERVAL_15 = 1;
export const SETTING_ALERT_INTERVAL_30 = 2;
export const SETTING_ALERT_INTERVAL_60 = 3;

// device only settings
export const DEVICE_SETTING_ALERT_SUPPRESS = "do_alertSuppress";
export const DEVICE_SETTING_TIME_OVERRIDE = "do_timeOverride";
export const DEVICE_SETTING_TIME_OVERRIDE_PLUS = "do_timeOverridePlus";
export const DEVICE_SETTING_TIME_OVERRIDE_HOUR = "do_timeOverrideHour";
export const DEVICE_SETTING_TIME_OVERRIDE_MIN = "do_timeOverrideMin";


// others
export const OVERRIDE_INDICATOR = "O";
