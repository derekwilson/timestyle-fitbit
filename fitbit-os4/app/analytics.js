import * as literals from "../common/literals";
import * as logging from "../common/logging";
//import analytics from "fitbit-google-analytics/app";
import analytics from "./analytics/ga";

let analyticsEnabled = false;

export function initialize(enabled, debugLogging) {
    logging.info(`analytics initialised ${enabled}`);
    if (enabled) {
        analyticsEnabled = true;
        analytics.configure({
            tracking_id: literals.ANALYTICS_TRACKING_ID,
            data_source: literals.ANALYTICS_DATA_SOURCE,
            debug_logging: debugLogging
          })
    }
}

export function sendAnalyticsEvent(category, action) {
    if (analyticsEnabled) {
        logging.info(`analytics sent ${category}, ${action}`);
        analytics.send({
            hit_type: "event",
            event_category: category,
            event_action: action
          })
    }
}

export function sendAnalyticsEventAndLabel(category, action, label) {
    if (analyticsEnabled) {
        logging.info(`analytics sent ${category}, ${action}, ${label}`);
        analytics.send({
            hit_type: "event",
            event_category: category,
            event_action: action,
            event_label: label
          })
    }
}




