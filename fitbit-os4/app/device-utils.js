import { memory } from "system";
import * as logging from "../common/logging";
import * as literals from "../common/literals";
import * as analytics from "./analytics"

export function logMemoryStats(message) {
  logging.info(`MEMORY: ${message} JS: ${memory.js.used} / ${memory.js.total}, Native: ${memory.native.used} / ${memory.native.total}`);
  if (memory.js.used > literals.JS_MEMORY_ALERT_LEVEL) {
    analytics.sendAnalyticsEventAndLabel(literals.AC_ERROR, literals.AA_MEMORY, memory.js.used);
  }
}
  
export function getJsMemoryUsed() {
  if (memory.js.used > literals.JS_MEMORY_ALERT_LEVEL) {
    logging.info(`JS Memory: ${memory.js.used}`);
    analytics.sendAnalyticsEventAndLabel(literals.AC_ERROR, literals.AA_MEMORY, memory.js.used);
  }
  return memory.js.used;
}
