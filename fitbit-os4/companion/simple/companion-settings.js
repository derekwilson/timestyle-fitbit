import * as logging from "../../common/logging";
import * as literals from "../../common/literals";
import * as emptyObjects from "../../common/objects";
import * as messaging from "messaging";
import { settingsStorage } from "settings";

let analyticsEvent = null;

export function initialize() {
  var defaultSettings = emptyObjects.default_settings();
  setDefaultSetting(literals.SETTING_TIME_COLOUR, defaultSettings[literals.SETTING_TIME_COLOUR], true);
  setDefaultSetting(literals.SETTING_BACKGROUND_COLOUR, defaultSettings[literals.SETTING_BACKGROUND_COLOUR], true);
  setDefaultSetting(literals.SETTING_PANEL_TEXT_COLOUR, defaultSettings[literals.SETTING_PANEL_TEXT_COLOUR], true);
  setDefaultSetting(literals.SETTING_PANEL_BACKGROUND_COLOUR, defaultSettings[literals.SETTING_PANEL_BACKGROUND_COLOUR], true);
  setDefaultSetting(literals.SETTING_ZERO_PAD_HOUR, defaultSettings[literals.SETTING_ZERO_PAD_HOUR], true);
  setDefaultSetting(literals.SETTING_ZERO_PAD_MONTH, defaultSettings[literals.SETTING_ZERO_PAD_MONTH], true);
  setDefaultSetting(literals.SETTING_ALERT_BT_STATUS_CHANGED, defaultSettings[literals.SETTING_ALERT_BT_STATUS_CHANGED], true);
  setDefaultSetting(literals.SETTING_SHOW_AMPM, defaultSettings[literals.SETTING_SHOW_AMPM], true);
  setDefaultSetting(literals.SETTING_NARROW_TIME_FONT, defaultSettings[literals.SETTING_NARROW_TIME_FONT], true);
  setDefaultSetting(literals.SETTING_SHOW_STEPS, defaultSettings[literals.SETTING_SHOW_STEPS], true);
  // I cannot work out how to set the default value for SETTING_TIME_FORMAT, SETTING_ALERT_INTERVAL and SETTING_PANEL_BOTTOM_DISPLAY

  let currentVersion = settingsStorage.getItem(literals.SETTING_VERSION);
  if (currentVersion !== JSON.stringify(defaultSettings[literals.SETTING_VERSION])) {
    logging.info(`version has changed from ${currentVersion} to ${defaultSettings[literals.SETTING_VERSION]}`);

    deferSendAnalytics(literals.AC_UPGRADE, defaultSettings[literals.SETTING_VERSION], currentVersion)
    // do any settings migration based on app version here
    // set the version as the migration is now done
    settingsStorage.setItem(literals.SETTING_VERSION, JSON.stringify(defaultSettings[literals.SETTING_VERSION]));
  }

  settingsStorage.addEventListener("change", evt => {
    sendAllValues();
  });
}

function setDefaultSetting(key, value, stringify) {
  let extantValue = settingsStorage.getItem(key);
  if (extantValue === null) {
    logging.debug(`need to set default for ${key} to ${value}`);
    if (stringify) {
      settingsStorage.setItem(key, JSON.stringify(value));
    } else {
      settingsStorage.setItem(key, value);
    }
  }
}

function sendAllValues() {
  var settingsObject = emptyObjects.default_settings();
  logging.verbose(`settings in storage ${settingsStorage.length}`);
  for (var index = 0; index<settingsStorage.length; index++) {
    settingsObject[settingsStorage.key(index)] = JSON.parse(settingsStorage.getItem(settingsStorage.key(index)));
  }
  sendSettingData(
    {
      settings: settingsObject
    }
  );
}

function deferSendAnalytics(category, action, label) {
  analyticsEvent =
    {
      analytics: {
        category: category,
        action: action,
        label: label
      }
    }
}

export function sendAnalytics(category, action, label) {
  sendSettingData(
    {
      analytics: {
        category: category,
        action: action,
        label: label
      }
    }
  );
}

function sendSettingData(data) {
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    logging.debug(`send settings ${JSON.stringify(data)}`);
    messaging.peerSocket.send(data);
  } else {
    logging.warning("No peerSocket connection");
  }
}

messaging.peerSocket.onopen = function() {
  logging.info("Message socket openned");
  if (analyticsEvent != null) {
    sendSettingData(analyticsEvent);
    analyticsEvent = null;
  }
};

