import { me } from "companion";

import * as literals from "../common/literals";
import * as logging from "../common/logging";
import * as simpleSettings from "./simple/companion-settings";
//import "fitbit-google-analytics/companion";
import "./analytics/ga";

console.log(`timestyle-fitbit companion v${literals.APP_VERSION} started`);

logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);
simpleSettings.initialize();

const MINUTE = 1000 * 60;
me.wakeInterval = 5 * MINUTE;

me.onwakeinterval = evt => {
    logging.debug("Companion was already awake - onwakeinterval");
}

if (me.launchReasons.wokenUp) { 
    // The companion started due to a periodic timer
    logging.debug("Started due to wake interval!");
}

if (me.launchReasons.peerAppLaunched) {
    // The Device application caused the Companion to start
    logging.debug("Device application was launched!")
}

if (me.launchReasons.settingsChanged) {
    // The companion was started due to application settings changes
    logging.debug("Settings were changed!")
}

