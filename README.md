# TimeStyle Fitbit

A completely free clockface for Fitbit OS watches inspired by the wonderful TimeStyle for Pebble

https://www.dantilden.com/projects/timestyle/

This is my implementation of Dan Tilden's wonderful design for Pebble watches. Its inspired by that project and attempts to reproduce that design on Fitbit OS watches. 

![Screen](support/listing/ionic/screen2.png)

I have tried to stay true to Dan's design goal of "using large, right-aligned digits against a timeline-style bar to show ancillary information". Making the display very clear and easy to see with minimal interactions in use.

It has a small subset of the functionality of the Pebble application.

You can

1. Set the colour of all watchface elements
1. Select 12 or 24 hour display, or use the setting from your fitbit account
1. Optional AM/PM indicator
1. Zero pad the date day
1. Zero pad the hour
1. Vibrate when the bluetooth status changes
1. See at a glance the device battery level.
1. Optionally display your steps, active zone minutes, distance, floors, or heart rate (heart rate does use extra power)
1. Periodic interval vibrate, with start and end hours for the alert
1. Wide or narrow time font (Versa2 users have said that the time is clipped on their devices)

Additional "on device" options, which can be accessed by tapping the side panel on right of the watchface. This means that the displayed time can be changed for different time zones when you have no internet access, for example when travelling.

1. Suppress alters from bluetooth status changes and the periodic interval alerts.
1. Adjust the time displayed on the watch face, by specifying an offset in hours and minutes.

## Requirement
### Local
- Node.js 8.+

### Fitbit Studio
- None

## Build
### Local

```sh
$ git clone https://bitbucket.org/derekwilson/timestyle-fitbit.git
$ cd fitbit-os4
timestyle-fitbit$ npm install
timestyle-fitbit$ npx fitbit
fitbit$ build
fitbit$ install
```

### Fitbit Studio
```sh
$ git clone https://bitbucket.org/derekwilson/timestyle-fitbit.git
```

